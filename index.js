
import fs from 'fs';
import pg from 'pg';

const connectionString = process.env.DATABASE_URL || 'postgres://postgres:nopass@127.0.0.1:5432/postgres';

const client = new pg.Client(connectionString);

const sql_query = `

SELECT
  (SELECT EXISTS (SELECT conname, conkey, conrelid, contype
                  FROM pg_constraint
                  WHERE pg_constraint.contype = 'p'
                    AND pg_constraint.conrelid = c.oid
                    AND f.attnum = ANY (pg_constraint.conkey))
                 ) AS primarykey,
  (SELECT EXISTS (SELECT conname, conkey, conrelid, contype
                  FROM pg_constraint
                  WHERE pg_constraint.contype = 'u'
                    AND pg_constraint.conrelid = c.oid
                    AND f.attnum = ANY (pg_constraint.conkey))
                 ) AS uniquekey,
  (SELECT EXISTS (SELECT conname, conkey, conrelid, contype
                  FROM pg_constraint
                  WHERE pg_constraint.contype = 'f'
                    AND pg_constraint.conrelid = c.oid
                    AND f.attnum = ANY (pg_constraint.conkey))
                 ) AS foreignkey,
  ((SELECT DISTINCT g.relname
     FROM pg_constraint
     LEFT JOIN pg_class AS g
     ON pg_constraint.confrelid = g.oid
     WHERE pg_constraint.contype = 'f'
       AND pg_constraint.conrelid = c.oid
       AND f.attnum = ANY (pg_constraint.conkey)
       AND length(g.relname) > 1
   )
  ) AS foreignkeyname,
  c.relname AS table_name,
  f.attnum  AS number,
  f.attname AS column_name,
  f.attnotnull AS notnull,
  pg_catalog.format_type(f.atttypid, f.atttypmod) AS type,
  c.relkind
FROM pg_attribute f
    JOIN pg_class c ON f.attrelid = c.oid
    JOIN pg_type  t ON f.atttypid = t.oid
    LEFT JOIN pg_namespace  n ON n.oid = c.relnamespace
WHERE
    (c.relkind = 'r'::char or c.relkind = 'm'::char or c.relkind = 'v'::char)
  AND
    n.nspname =  'public'
  AND
    f.attnum > 0
ORDER BY c.relname, number

`;

client.connect();
const query = client.query(
  sql_query, [], function(err, result) {

    if(err) {
      return console.error('error running query', err);
    }

    var file_out = `\n\n
@startuml\n
' uncomment the line below if you're using computer with a retina display
' skinparam dpi 300
!define MaterialView(name,desc) class name as "desc" << (M,#AAAAFF) >>
!define Table(name,desc) class name as "desc" << (T,#FFAAAA) >>
!define View(name,desc) class name as "desc" << (V,#AAAAFF) >>
' we use bold for primary key
' green color for unique
' and underscore for not_null
!define primary_key(x) <b>x</b>
!define unique(x) <color:green>x</color>
!define not_null(x) <u>x</u>
' other tags available:
' <i></i>
' <back:COLOR></color>, where color is a color name or html color code
' (#FFAACC)
' see: http://plantuml.com/classes.html#More
hide methods
hide stereotypes\n
' entities\n
`;
    let foreignkeyRelations = '';

    let cur_table = '';
    result.rows.map((row) => {

      if(row.table_name !== cur_table) {
        if ('' !== cur_table) {
          // end the old table
          file_out += '}\n\n';
        }
        // start the new table/view
        if(row.relkind === 'm') {
          file_out += `MaterialView(${row.table_name}, ${row.table_name}) {\n`;
        }
        else if(row.relkind === 'v') {
          file_out += `View(${row.table_name}, ${row.table_name}) {\n`;
        }
        else { // r is normal table
          file_out += `Table(${row.table_name}, ${row.table_name}) {\n`;
        }
        cur_table = row.table_name;
      }

      let row_data = '';

      row.type = row.type.replace('(', ' ');
      row.type = row.type.replace(')', '');

      if(row.primarykey === true) {
        row_data = `primary_key(${row.column_name}) "${row.type} PK`;
      }
      else if((row.uniquekey === true) && (row.notnull === true)) {
        row_data = `not_null(unique(${row.column_name})) "${row.type}`;
      }
      else if(row.uniquekey === true) {
        row_data = `unique(${row.column_name}) "${row.type}`;
      }
      else if(row.notnull === true) {
        row_data = `not_null(${row.column_name}) "${row.type}`;
      }
      else {
        row_data = `${row.column_name} "${row.type}`;
      }

      if(row.foreignkey) {
        row_data += ' FK';
        foreignkeyRelations += `${row.table_name} --> ${row.foreignkeyname}\n`
      }
      row_data += '"\n';

      file_out += `${row_data}`;
    });

    // close the last table
    file_out += "}\n\n";

    // print out relations
    file_out += "' relationships\n\n";
    file_out += foreignkeyRelations;

    file_out += `\n\n
legend
|=  Legend: |
| primary_key(Primary Key - bold) |
| unique(unique - green) |
| not_null(not null - underline) |
endlegend\n
@enduml
`;
    fs.writeFileSync('erd.puml', file_out);

  }
);

query.on('end', () => { client.end(); });
